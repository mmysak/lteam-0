using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public GameObject go;
    public AudioSource audioSource;
    void Start()
    {
        if (go)
            DontDestroyOnLoad(go);
    }
    public void PlaySong(AudioClip audio) {
        audioSource.PlayOneShot(audio, 1);
    }
}
