using Cinemachine;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class GameCore : MonoBehaviour {
	public MouseController mouseController;
	public GameObject szafaUI, marketUI, handsUI;
    public AudioSource audioSource;
	public AudioClip buy_sell, music_bg,loading_music;
    public int coins;
	public Text CoinsText;
	public Camera mainCamera;
	public VirtualCursor vc;
	[HideInInspector]
	public InventorySlot olds, SelectSlot;
	public InventorySlot handLeft_slot, handRight_slot;
	public Transform hand_left, hand_right;
	public CinemachineVirtualCamera cmvc;

	public Image buy_button, buy_icon;
	public Text buy_price;
	public Transform SlotsContainer;
	public ItemScriptableObject[] iitems;
	int id_hash;
	float _dist;
	Ray _ray;
	RaycastHit _hit;
    private void Awake() {
        Screen.SetResolution(1920, 1080, true);
        audioSource.clip = music_bg;
		audioSource.Play();
    }
    void Start() {
		RandomizeShop();
		StartCoroutine(AfterStart());
	}
	IEnumerator AfterStart() {
		yield return new WaitForSeconds(.2f);
		CoinsText.text = coins.ToString();
		VirtualCursor.cursor_control = false;
		szafaUI.SetActive(false);
		marketUI.SetActive(false);
	}
	public void RandomizeShop() {
        for (int i = 0; i < SlotsContainer?.childCount - 1; i++) {
            InventorySlot slot = SlotsContainer.GetChild(i).GetComponent<InventorySlot>();
            slot.item = iitems[Random.Range(0, iitems.Length)];
            slot.amount = Random.Range(1, slot.item.max);
            slot.priceText.text = slot.item.price.ToString();
			slot.SetIcon();
        }
    }
	private void Update() {

		if (Input.GetMouseButton(0)) {
			Physics.Raycast(_ray, out _hit, 50f);
			if (_hit.collider != null) {
				Vector3 position1XZ = new Vector3(mainCamera.transform.parent.position.x, 0, mainCamera.transform.parent.position.z);
				Vector3 position2XZ = new Vector3(_hit.transform.position.x, 0, _hit.transform.position.z);

				_dist = Vector3.Distance(position1XZ, position2XZ);
				if (_dist < 50) {
					WhatIsThis GO = _hit.collider?.gameObject.GetComponent<WhatIsThis>();
					if (GO) {
						//print(GO.what);
						switch (GO.what) {
							case What.szafa:
								//open ui for szafu
								cmvc.enabled = false;
								szafaUI.SetActive(true);
								VirtualCursor.cursor_control = true;
								handsUI.SetActive(true);
								break;
							case What.market:
								cmvc.enabled = false;
								marketUI.SetActive(true);
								VirtualCursor.cursor_control = true;
								handsUI.SetActive(true);
								break;
						}
					}
				}
			}
		}
	}
	public bool Interact() {
		_ray = mainCamera.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));
		if (Physics.Raycast(_ray, out _hit, 50f)) {
			WhatIsThis GO = _hit.collider?.gameObject.GetComponent<WhatIsThis>();
			if (GO) {
				//print(GO.what);
				switch (GO.what) {
					case What.szafa:
						//open ui for szafu
						cmvc.enabled = false;
						szafaUI.SetActive(true);
						VirtualCursor.cursor_control = true;
						handsUI.SetActive(true);
						return true;
					case What.market:
						cmvc.enabled = false;
						marketUI.SetActive(true);
						VirtualCursor.cursor_control = true;
						handsUI.SetActive(true);
						return true;
				}
			}
			else {
				return false;
			}
		}
		return false;
	}
	void SpawnitemToHand(ItemScriptableObject _item, int _amount, int handID) {
		if (handID == 0) {//left hand
			handLeft_slot.item = _item;
			handLeft_slot.amount = _amount;
			handLeft_slot.Empty = false;
			handLeft_slot.SetIcon();
		}
		else {
			handRight_slot.item = _item;
			handRight_slot.amount = _amount;
			handRight_slot.Empty = false;
			handRight_slot.SetIcon();
		}
		UpdateItemsInHands();
	}
	public void UpdateItemsInHands() {
		for (int i = 0; i < hand_right.childCount; i++) {
			hand_right?.GetChild(i).gameObject.SetActive(false);
			hand_left?.GetChild(i).gameObject.SetActive(false);
		}
		if (handRight_slot.item)
			hand_right.GetChild(handRight_slot.item.id)?.gameObject.SetActive(true);
		if (handLeft_slot.item) {
			hand_left.GetChild(handLeft_slot.item.id)?.gameObject.SetActive(true);
		}
	}
	public void ShowDetailsOfBuying(int id) {
		InventorySlot islot = SlotsContainer.GetChild(id).GetComponent<InventorySlot>();
		buy_button.GetComponentInChildren<Text>().text = islot.IsBuyingMarketSlot ? "Buy" : "Sell";
		if (islot.IsBuyingMarketSlot)
			buy_button.color = ((handRight_slot.item == null || handLeft_slot.item == null) && (islot.item.price * islot.amount >= coins) ? Color.green : Color.red);
		else
			buy_button.color = ((handRight_slot.item == islot.item || handLeft_slot.item == islot.item) ? Color.green : Color.red);
		id_hash = id;
		buy_icon.sprite = islot.item.icon;
		buy_price.text = (islot.item.price * islot.amount).ToString() + " (" + (islot.item.price * islot.amount).ToString() + ")";
    }
    public void PlaySong(AudioClip audio) {
        audioSource.PlayOneShot(audio, 1);
    }
    public void buy() {
		InventorySlot targetSlot = SlotsContainer.GetChild(id_hash).GetComponent<InventorySlot>();
		if (targetSlot.IsBuyingMarketSlot) {
			bool haveEmptyHand = (handRight_slot.item == null || handLeft_slot.item == null);
			if (haveEmptyHand) {
				InventorySlot islot = SlotsContainer.GetChild(id_hash).GetComponent<InventorySlot>();
				if (!islot.Empty) {
					if (coins - islot.item.price * islot.amount >= 0) {
						PlaySong(buy_sell);
                        coins -= islot.item.price * islot.amount;
						CoinsText.text = coins.ToString();
						SpawnitemToHand(islot.item, islot.amount, (handLeft_slot.item == null ? 0 : 1));
						islot.AmountText.text = "buyed";
						islot.priceText.text = "";
						islot.Empty = true;
					}
				}
			}
		}
		else {
			bool haveItemInHands = false;
			int hand_ID = 0;
			if ((haveItemInHands = handLeft_slot.item == targetSlot.item && handLeft_slot.item != null) || (haveItemInHands = handRight_slot.item == targetSlot.item && handRight_slot.item != null)) hand_ID = handLeft_slot.item == targetSlot.item ? 0 : 1;
			if (haveItemInHands) {
				if (!targetSlot.Empty) {
					InventorySlot handSlot = hand_ID == 0 ? handLeft_slot : handRight_slot;
					if (targetSlot.amount < handSlot.amount) {
                        PlaySong(buy_sell);
                        coins += targetSlot.item.price * targetSlot.amount;
						handSlot.amount -= targetSlot.amount;
						handSlot.AmountText.text = handSlot.amount.ToString();
						targetSlot.amount = 0;
						targetSlot.AmountText.text = "selled";
						targetSlot.priceText.text = "";
						targetSlot.Empty = true;
					}
					else {
                        PlaySong(buy_sell);
                        coins += handSlot.item.price * handSlot.amount;
						targetSlot.amount -= handSlot.amount;
						targetSlot.AmountText.text = targetSlot.amount.ToString();
						handSlot.amount = 0;
						handSlot.AmountText.text = "";
						handSlot.Empty = true;
						handSlot.item = null;
						handSlot.icon.color = new Color(1, 1, 1, 0);
					}
					CoinsText.text = coins.ToString();
				}
			}
		}
	}
	public void Close(GameObject go) {
		cmvc.enabled = true;
		go.SetActive(false);
		handsUI.SetActive(false);
		VirtualCursor.cursor_control = false;
		UpdateItemsInHands();
	}
}