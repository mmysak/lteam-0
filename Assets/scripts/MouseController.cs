using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MouseController : MonoBehaviour
{
    public Transform player;
    [HideInInspector] public Vector3 middle_of_screen;
    [HideInInspector] public bool first_update;
    public VirtualCursor VirtualCursor;
    private bool last_CC;
    public Camera Camera;
    public GameObject Camera2;
    // Start is called before the first frame update
    Vector3 input_direction, _direction;
    public Vector2 MouseSensivity;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
        middle_of_screen = new Vector3(Screen.width/2, Screen.height/2, 0);
        Mouse.current.WarpCursorPosition(new Vector2(middle_of_screen.x, middle_of_screen.y));
    }

    // Update is called once per frame
    void Update() 
    {
        VirtualCursor.enabled = !VirtualCursor.cursor_control;
        if (VirtualCursor.cursor_control != last_CC) 
        {
            print("reset");
            HUB_Interaction();
        }
        if (first_update == true && Cursor.lockState == CursorLockMode.Confined && !last_CC)
        {
            Mouse_Movement();
            Character_Movement();
        }
        else
        {
            first_update = true; // Just a prevention so that Virtual Cursor doesn't snap out of view after first frame
        }
        if (Keyboard.current.escapeKey.isPressed && Cursor.lockState == CursorLockMode.Confined)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else if (Keyboard.current.escapeKey.isPressed && Cursor.lockState == CursorLockMode.None)
        {
            Mouse.current.WarpCursorPosition(new Vector2(middle_of_screen.x, middle_of_screen.y));
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;
        }
        last_CC = VirtualCursor.cursor_control;
    }
    void HUB_Interaction()
    {
        Mouse.current.WarpCursorPosition(new Vector2(middle_of_screen.x, middle_of_screen.y));
        Cursor.visible = VirtualCursor.cursor_control;
        VirtualCursor.enabled = !VirtualCursor.cursor_control;
    }
    void Mouse_Movement()
    {
        //print(Input.mousePosition - middle_of_screen);
        Camera2.transform.Rotate(0,(Input.mousePosition.x - middle_of_screen.x) * MouseSensivity.x,0);
        Camera.transform.Rotate(-(Input.mousePosition.y - middle_of_screen.y) * MouseSensivity.y,0,0);
        Mouse.current.WarpCursorPosition(new Vector2(middle_of_screen.x, middle_of_screen.y));
        Camera.transform.localRotation = Quaternion.Euler(Mathf.Clamp(WrapAngle(Camera.transform.eulerAngles.x), -40.0f, 45.0f), 0, 0);
    }
    void Character_Movement()
    {
        input_direction.z = System.Convert.ToInt32(Keyboard.current.wKey.isPressed) - System.Convert.ToInt32(Keyboard.current.sKey.isPressed); //Forward/Backward
        input_direction.x = System.Convert.ToInt32(Keyboard.current.dKey.isPressed) - System.Convert.ToInt32(Keyboard.current.aKey.isPressed); //Right/Left
        _direction = Vector3.zero;
        _direction += input_direction.z * player.forward;
        _direction += input_direction.x * player.right;
        player.position += _direction * 4;
    }
    float WrapAngle(float angle) {
        angle %= 360;
        if (angle > 180) {
            return angle - 360;
        }
        return angle;
    }
    float UnwrapAngle(float angle) {
        if (angle >= 0) {
            return angle;
        }
        angle = angle % 360;
        return 360-angle;
    }
}
