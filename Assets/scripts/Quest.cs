using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest
{
    public string[] ConditionType;
    public string[] Condition;
    public Objectives[] Objectives;
    public string[] Contents;
    public string[] Objective_comperison;
    public string[] Items_to_force;
    public int[] Where_to_force;
    public int[] When_to_force;
}
