using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
// Oto co miałem na myśli mówiąc o tutorialu/questing systemie
// Na razie wszystko trzeba wprowadzać ręcznie
public class Tutorial : MonoBehaviour
{
    public TextAsset Quests;
    public Text Tutorial_text;
    public Image Self_image;
    public Objectives[] objectives;
    private bool[] active_objectives;
    public string[] contents;
    private bool[] progress;
    private int progress_iterator;
    public string[] progress_comparator;
    private float progress_acumulator;
    //[HideInInspector] 
    public bool active;
    private bool tmp_bool_acu;
    public MouseController mouseController;
    public GameObject Market, Szafka;
    public GameCore gameCore;
    private Quest holder;
    private int coins_cointainer;
    private bool[] itemz;
    void Start()
    {
        //active = System.Convert.ToBoolean(PlayerPrefs.GetInt("StartTutorial"));
        active = PlayerPrefs.GetInt("StartTutorial") ==1? active= true: active = false;
        if (active)
        {
            holder = JsonUtility.FromJson<Quest>(Quests.text);
            Activate_Objectives();
            Read_Objectives();
            Start_tutorial();
        }
        else
        {
            Shutdown_tutorial();
        }
    }

    void Update()
    {
        for (int i = 0; i < holder.When_to_force.Length;i++){
            if (holder.When_to_force[i] == progress_iterator){
                InventorySlot slot = gameCore.SlotsContainer.GetChild(holder.Where_to_force[i]).GetComponent<InventorySlot>();
                slot.item = gameCore.iitems[System.Convert.ToInt32(holder.Items_to_force[i].Substring(4))];
			    slot.amount = 1;
			    slot.priceText.text = slot.item.price.ToString();
                slot.SetIcon();
            }
        }
        if (!active){
            active = System.Convert.ToBoolean(PlayerPrefs.GetInt("StartTutorial"));
            Start();
        }
        try {
            if (System.Convert.ToInt32(progress_comparator[progress_iterator]) > progress_acumulator && objectives.Length >= 1 && mouseController.first_update)
            {
                switch (objectives[progress_iterator]){
                    case Objectives.Any_Character_Movement:
                        if (active_objectives[(int)Objectives.Any_Character_Movement] == true){
                            Any_Character_Movement();
                        }else{
                            print("Objective Any_Character_Movement is disabled.");
                        }
                        break;
                    case Objectives.Character_Movement_Forward:
                        if (active_objectives[(int)Objectives.Character_Movement_Forward] == true){
                            Character_Movement_Forward();
                        }else{
                            print("Objective Character_Movement_Forward is disabled.");
                        }
                        break;
                    case Objectives.Character_Movement_Backward:
                        if (active_objectives[(int)Objectives.Character_Movement_Backward] == true){
                            Character_Movement_Backward();
                        }else{
                            print("Objective Character_Movement_Backward is disabled.");
                        }
                        break;
                    case Objectives.Character_Movement_Left:
                        if (active_objectives[(int)Objectives.Character_Movement_Left] == true){
                            Character_Movement_Left();
                        }else{
                            print("Objective Character_Movement_Left is disabled.");
                        }
                        break;
                    case Objectives.Character_Movement_Right:
                        if (active_objectives[(int)Objectives.Character_Movement_Right] == true){
                            Character_Movement_Right();
                        }else{
                            print("Objective Character_Movement_Right is disabled.");
                        }
                        break;
                    case Objectives.Any_Mouse_Movement:
                        if (active_objectives[(int)Objectives.Any_Mouse_Movement] == true){
                            Any_Mouse_Movement();
                        }else{
                            print("Objective Any_Mouse_Movement is disabled.");
                        }
                        break;
                    case Objectives.Mouse_Movement_Up:
                        if (active_objectives[(int)Objectives.Mouse_Movement_Up] == true){
                            Mouse_Movement_Up();
                        }else{
                            print("Objective Mouse_Movement_Up is disabled.");
                        }
                        break;
                    case Objectives.Mouse_Movement_Down:
                        if (active_objectives[(int)Objectives.Mouse_Movement_Down] == true){
                            Mouse_Movement_Down();
                        }else{
                            print("Objective Mouse_Movement_Down is disabled.");
                        }
                        break;
                    case Objectives.Mouse_Movement_Left:
                        if (active_objectives[(int)Objectives.Mouse_Movement_Left] == true){
                            Mouse_Movement_Left();
                        }else{
                            print("Objective Mouse_Movement_Left is disabled.");
                        }
                        break;
                    case Objectives.Mouse_Movement_Right:
                        if (active_objectives[(int)Objectives.Mouse_Movement_Right] == true){
                            Mouse_Movement_Right();
                        }else{
                            print("Objective Mouse_Movement_Right is disabled.");
                        }
                        break;
                    case Objectives.Market_Open:
                        if (active_objectives[(int)Objectives.Market_Open] == true){
                            Market_Open();
                        }else{
                            print("Objective Market_Open is disabled.");
                        }
                        break;
                    case Objectives.Market_Close:
                        if (active_objectives[(int)Objectives.Market_Close] == true){
                            Market_Close();
                        }else{
                            print("Objective Market_Close is disabled.");
                        }
                        break;
                    case Objectives.Market_Open_Close:
                        if (active_objectives[(int)Objectives.Market_Open_Close] == true){
                            Market_Open_Close();
                        }else{
                            print("Objective Market_Open_Close is disabled.");
                        }
                        break;
                    case Objectives.Szafka_Open:
                        if (active_objectives[(int)Objectives.Szafka_Open] == true){
                            Szafka_Open();
                        }else{
                            print("Objective Szafka_Open is disabled.");
                        }
                        break;
                    case Objectives.Szafka_Close:
                        if (active_objectives[(int)Objectives.Szafka_Close] == true){
                            Szafka_Close();
                        }else{
                            print("Objective Szafka_Close is disabled.");
                        }
                        break;
                    case Objectives.Szafka_Open_Close:
                        if (active_objectives[(int)Objectives.Szafka_Open_Close] == true){
                            Szafka_Open_Close();
                        }else{
                            print("Objective Szafka_Open_Close is disabled.");
                        }
                        break;
                    case Objectives.Coins_Amount:
                        if (active_objectives[(int)Objectives.Coins_Amount] == true){
                            Coins_Amount();
                        }else{
                            print("Objective Coins_Amount is disabled.");
                        }
                        break;
                    case Objectives.Test:
                        progress_acumulator++;
                        //print("Works!");
                        break;
                }
            }
            else
            {
                progress_acumulator = 0;
                progress[progress_iterator] = true;
                progress_iterator++;
                if (progress_iterator < progress.Length)
                {
                    Shutdown_tutorial();
                }
            }
        }
        catch (System.Exception e){
            switch (objectives[progress_iterator]){
                case Objectives.Market_Buy:
                    if (active_objectives[(int)Objectives.Market_Buy] == true){
                        Market_Buy();
                    }else{
                        print("Objective Market_Buy is disabled.");
                    }
                    break;
                case Objectives.Market_Sell:
                    if (active_objectives[(int)Objectives.Market_Sell] == true){
                        Market_Sell();
                    }else{
                        print("Objective Market_Sell is disabled.");
                    }
                    break;
                case Objectives.Szafka_Place:
                    if (active_objectives[(int)Objectives.Szafka_Place] == true){
                        Szafka_Place();
                    }else{
                        print("Objective Szafka_Place is disabled.");
                    }
                    break;
                case Objectives.Szafka_Take:
                    if (active_objectives[(int)Objectives.Szafka_Take] == true){
                        Szafka_Take();
                    }else{
                        print("Objective Szafka_Take is disabled.");
                    }
                    break;
                default:
                    print("Fuckup");
                    break;
            }
            print(e);

        }
        Tutorial_text.text = contents[progress_iterator].ToString();
    }
    void Activate_Objectives(){
        Mouse.current.WarpCursorPosition(new Vector2(mouseController.middle_of_screen.x, mouseController.middle_of_screen.y));
        active_objectives = new bool[Objectives.GetNames(typeof(Objectives)).Length];
        itemz = new bool[gameCore.hand_left.childCount*2];
        coins_cointainer = gameCore.coins;
        for (int i = 0; i < holder.ConditionType.Length; i++){
            Object property = (Object)this.GetType().GetField(holder.Condition[i]).GetValue(this);
            switch (holder.ConditionType[i]){
                case "GameObjectReference":
                    switch (property.name){
                        case "MarketUI":
                            active_objectives[(int)Objectives.Market_Buy] = true;
                            active_objectives[(int)Objectives.Market_Sell] = true;
                            active_objectives[(int)Objectives.Market_Open] = true;
                            active_objectives[(int)Objectives.Market_Close] = true;
                            active_objectives[(int)Objectives.Market_Open_Close] = true;
                            break;
                        case "SzafaUI":
                            active_objectives[(int)Objectives.Szafka_Open] = true;
                            active_objectives[(int)Objectives.Szafka_Close] = true;
                            active_objectives[(int)Objectives.Szafka_Open_Close] = true;
                            break;
                        case "GarnekUI":
                            print("OK?");
                            break;
                        case "GameCore":
                            active_objectives[(int)Objectives.Coins_Amount] = true;
                            break;
                        case "Cursor":
                            active_objectives[(int)Objectives.Any_Character_Movement] = true;
                            active_objectives[(int)Objectives.Character_Movement_Backward] = true;
                            active_objectives[(int)Objectives.Character_Movement_Forward] = true;
                            active_objectives[(int)Objectives.Character_Movement_Left] = true;
                            active_objectives[(int)Objectives.Character_Movement_Right] = true;
                            active_objectives[(int)Objectives.Any_Mouse_Movement] = true;
                            active_objectives[(int)Objectives.Mouse_Movement_Down] = true;
                            active_objectives[(int)Objectives.Mouse_Movement_Left] = true;
                            active_objectives[(int)Objectives.Mouse_Movement_Right] = true;
                            active_objectives[(int)Objectives.Mouse_Movement_Up] = true;
                            break;
                    }
                    break;
                case "AllOn":
                    for (int j = 0; j < active_objectives.Length; j++){
                        active_objectives[j] = true;
                    }
                    break;
            }
        }
    }
    void Read_Objectives(){
        objectives = new Objectives[holder.Objectives.Length];
        objectives = holder.Objectives;
        contents = new string[holder.Contents.Length];
        contents = holder.Contents;
        progress_comparator = new string[holder.Objective_comperison.Length];
        progress_comparator = holder.Objective_comperison;
    }
    void Start_tutorial()
    {
        Tutorial_text.gameObject.SetActive(active);
        Self_image.gameObject.SetActive(active);
        progress_iterator = 0;
        progress = new bool[contents.Length];
        for (int i = 0; i < contents.Length; i++)
        {
            progress[i] = false;
        }
    }
    private void Shutdown_tutorial(){
        this.gameObject.SetActive(active);
    }
    private void Any_Character_Movement(){
        progress_acumulator += System.Convert.ToInt32(Keyboard.current.wKey.isPressed) + System.Convert.ToInt32(Keyboard.current.sKey.isPressed) + System.Convert.ToInt32(Keyboard.current.dKey.isPressed) + System.Convert.ToInt32(Keyboard.current.aKey.isPressed);
    }
    private void Character_Movement_Forward(){
        progress_acumulator += System.Convert.ToInt32(Keyboard.current.wKey.isPressed);
    }
    private void Character_Movement_Backward(){
        progress_acumulator += System.Convert.ToInt32(Keyboard.current.sKey.isPressed);
    }
    private void Character_Movement_Left(){
        progress_acumulator += System.Convert.ToInt32(Keyboard.current.aKey.isPressed);
    }
    private void Character_Movement_Right(){
        progress_acumulator += System.Convert.ToInt32(Keyboard.current.dKey.isPressed);
    }
    private void Any_Mouse_Movement(){
        progress_acumulator += Mathf.Abs(Input.mousePosition.x - mouseController.middle_of_screen.x) + Mathf.Abs(Input.mousePosition.y - mouseController.middle_of_screen.y);
    }
    private void Mouse_Movement_Up(){
        if (Input.mousePosition.y - mouseController.middle_of_screen.y < 0){
            progress_acumulator += Mathf.Abs(Input.mousePosition.y - mouseController.middle_of_screen.y);
        }
    }
    private void Mouse_Movement_Down(){
        if (Input.mousePosition.y - mouseController.middle_of_screen.y > 0){
            progress_acumulator += Mathf.Abs(Input.mousePosition.y - mouseController.middle_of_screen.y);
        }
    }
    private void Mouse_Movement_Left(){
        if (Input.mousePosition.x - mouseController.middle_of_screen.x < 0){
            progress_acumulator += Mathf.Abs(Input.mousePosition.x - mouseController.middle_of_screen.x);
        }
    }
    private void Mouse_Movement_Right(){
        if (Input.mousePosition.x - mouseController.middle_of_screen.x > 0){
            progress_acumulator += Mathf.Abs(Input.mousePosition.x - mouseController.middle_of_screen.x);
        }
    }
    private void Market_Open(){
        if (Market.activeSelf != tmp_bool_acu && Market.activeSelf == true){
            progress_acumulator++;
        }
        tmp_bool_acu = Market.activeSelf;
    }
    private void Market_Close(){
        if (Market.activeSelf != tmp_bool_acu && Market.activeSelf == false){
            progress_acumulator++;
        }
        tmp_bool_acu = Market.activeSelf;
    }
    private void Market_Open_Close(){
        if (Market.activeSelf != tmp_bool_acu){
            gameCore.RandomizeShop();
            progress_acumulator++;
        }
        tmp_bool_acu = Market.activeSelf;
    }
    private void Market_Buy(){
        print(progress_comparator[progress_iterator].Substring(4));
        int num = Hand_Changed();
        if (num >= 0 && coins_cointainer > gameCore.coins && gameCore.iitems[num] == gameCore.iitems[System.Convert.ToInt32(progress_comparator[progress_iterator].Substring(4))]){
            progress_iterator++;
        }
    }
    private void Market_Sell(){
        int num = Hand_Changed();
        if (num >= 0 && coins_cointainer < gameCore.coins && gameCore.iitems[num] == gameCore.iitems[System.Convert.ToInt32(progress_comparator[progress_iterator].Substring(4))]){
            progress_iterator++;
        }
    }
    private void Szafka_Open(){
        if (Szafka.activeSelf != tmp_bool_acu && Szafka.activeSelf == true){
            progress_acumulator++;
        }
        tmp_bool_acu = Szafka.activeSelf;
    }
    private void Szafka_Close(){
        if (Szafka.activeSelf != tmp_bool_acu && Szafka.activeSelf == false){
            progress_acumulator++;
        }
        tmp_bool_acu = Szafka.activeSelf;
    }
    private void Szafka_Open_Close(){
        if (Szafka.activeSelf != tmp_bool_acu){
            progress_acumulator++;
        }
        tmp_bool_acu = Szafka.activeSelf;
    }
    private void Coins_Amount(){
        progress_acumulator = gameCore.coins;
    }
    private void Szafka_Place(){
        int num = Hand_Changed();
        if (num >= 0 && gameCore.iitems[num] == gameCore.iitems[System.Convert.ToInt32(progress_comparator[progress_iterator].Substring(4))]){
            progress_iterator++;
        }
    }
    private void Szafka_Take(){
        int num = Hand_Changed();
        if (num >= 0 && gameCore.iitems[num] == gameCore.iitems[System.Convert.ToInt32(progress_comparator[progress_iterator].Substring(4))]){
            progress_iterator++;
        }
    }
    private int Hand_Changed(){
        for (int i = 0; i < gameCore.hand_left.childCount; i++){
            if (itemz[i] != gameCore.hand_left.GetChild(i).gameObject.activeSelf)
            {
                itemz[i] = gameCore.hand_left.GetChild(i).gameObject.activeSelf;
                return i;
            } 
            else if (itemz[i+8] != gameCore.hand_right.GetChild(i).gameObject.activeSelf)
            {
                itemz[i+8] = gameCore.hand_right.GetChild(i).gameObject.activeSelf;
                return i;
            }
        }
        return -1;
    }
}
public enum Objectives {
    Test,
    Any_Character_Movement,
    Character_Movement_Forward,
    Character_Movement_Backward,
    Character_Movement_Left,
    Character_Movement_Right,
    Any_Mouse_Movement,
    Mouse_Movement_Up,
    Mouse_Movement_Down,
    Mouse_Movement_Left,
    Mouse_Movement_Right,
    Market_Open,
    Market_Close,
    Market_Open_Close,
    Market_Buy, //brak implementacji
    Market_Sell, // brak implementacji
    Szafka_Open,
    Szafka_Close,
    Szafka_Open_Close,
    Szafka_Place,
    Szafka_Take,
    Coins_Amount,
}