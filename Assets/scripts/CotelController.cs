using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class Requieds {
	public ItemScriptableObject item1, item2;
	public string funcName;
}
public class CotelController : MonoBehaviour {
	public ItemScriptableObject currentItem1, currentItem2;
	public GameObject slots,LoadingScreen,canvas;
	public Requieds[] requieds;
    public AudioSource audioSource,loadingSource;
    public AudioClip menu_music, loading_music;
	int time;
    private void Awake() {
		Screen.SetResolution(1920, 1080,true);
        audioSource.clip = menu_music;
        audioSource.Play();
    }
    public void AddItem(ItemScriptableObject itm) {
		if (currentItem1 == null) {
			currentItem1 = itm;
			slots.transform.GetChild(0).GetComponent<Image>().color = new Color(1,1,1,1);
			slots.transform.GetChild(0).GetComponent<Image>().sprite = itm.icon;
		}
		else if (currentItem1 != null) {
			currentItem2 = itm;
			slots.transform.GetChild(1).GetComponent<Image>().color = new Color(1, 1, 1, 1);
			slots.transform.GetChild(1).GetComponent<Image>().sprite = itm.icon;
			StartCoroutine("ResetAll");
			Action();
		}
	}
	public void Action() {
		for (int i = 0; i < requieds.Length; i++) {
			if (currentItem1 == requieds[i].item1) {
				if (currentItem2 == requieds[i].item2) {
					StartCoroutine(requieds[i].funcName);
				}
			}
		}
	}
	void LoadingController() {
        loadingSource.clip = loading_music;
        loadingSource.Play();
        canvas.SetActive(false);
		LoadingScreen.SetActive(true);
	}
	IEnumerator StartGame() {
		LoadingController();
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("game");
		asyncLoad.allowSceneActivation = false;
		while (asyncLoad.isDone) yield return new WaitForSeconds(.1f);
		yield return new WaitForSeconds(2f);
		asyncLoad.allowSceneActivation = true;
		yield return null;
	}
	IEnumerator Settings() {
		print("Coming soon...");
		yield return null;
	}
	IEnumerator StartTutorial() {
		PlayerPrefs.SetInt("StartTutorial", 1);
		StartCoroutine(StartGame());
		yield return null;
	}
	IEnumerator StartNoTutorial() {
        PlayerPrefs.SetInt("StartTutorial", 0);
        StartCoroutine(StartGame());
        yield return null;
    }

    IEnumerator ExitGame() {
		Application.Quit();
		yield return null;
	}
	IEnumerator ResetAll() {
		Image slot0 = slots.transform.GetChild(0).GetComponent<Image>();
		Image slot1 = slots.transform.GetChild(1).GetComponent<Image>();
		while (slot0.color.a > 0) {
			slot0.color = new Color(1, 1, 1, (slot0.color.a) - 0.05f);
			slot1.color = new Color(1, 1, 1, (slot0.color.a) - 0.05f);
			yield return new WaitForSeconds(0.075f);
		}
		slot0.color = new Color(1, 1, 1, 0);
		slot1.color = new Color(1, 1, 1, 0);
		currentItem1 = null;
		currentItem2 = null;
		yield return null;
	}
}