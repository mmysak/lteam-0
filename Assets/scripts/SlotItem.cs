using UnityEngine;
[CreateAssetMenu(fileName = "market", menuName = "New market Item")]
public class SlotItem : ScriptableObject {
	public FoodItem item;
	public int amount;
}
