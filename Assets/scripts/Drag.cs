using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class Drag : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler {
	public GameCore inventory;
	public InventorySlot oldSlot;
	public void OnDrag(PointerEventData eventData) {
		if (oldSlot.Empty || oldSlot.IsSellingMarketSlot || oldSlot.IsBuyingMarketSlot) return;
		GetComponent<RectTransform>().position += new Vector3(eventData.delta.x, eventData.delta.y);
	}
	public void OnEndDrag(PointerEventData eventData) {
		if (eventData.pointerCurrentRaycast.gameObject == null) return;
		if (eventData.pointerCurrentRaycast.gameObject.name == "SzafaUI") return;
		inventory.olds = eventData.pointerCurrentRaycast.gameObject.transform.parent.parent.GetComponent<InventorySlot>();
	}
	public void OnPointerDown(PointerEventData eventData) {
		if (eventData.pointerCurrentRaycast.gameObject == null || oldSlot.IsSellingMarketSlot || oldSlot.IsBuyingMarketSlot) return;
		inventory.olds = eventData.pointerCurrentRaycast.gameObject.transform.parent.GetComponent<InventorySlot>();
		if (oldSlot.Empty) return;
		GetComponentInChildren<Image>().raycastTarget = false;
		oldSlot.GetComponent<Image>().raycastTarget = true;
		if (transform.parent.parent.parent.name == "Canvas") transform.SetParent(transform.parent.parent.parent);
		else transform.SetParent(transform.parent.parent);
	}
	public void OnPointerUp(PointerEventData eventData) {
		transform.SetParent(oldSlot.transform);
		transform.position = oldSlot.transform.position;
		GetComponentInChildren<Image>().raycastTarget = true;
		oldSlot.GetComponent<Image>().raycastTarget = false;
		if (eventData.pointerCurrentRaycast.gameObject == null) return;
		if (eventData.pointerCurrentRaycast.gameObject.name == "SzafaUI") return;
		InventorySlot cs = eventData.pointerCurrentRaycast.gameObject.GetComponent<InventorySlot>();
		if (cs == null) cs = eventData.pointerCurrentRaycast.gameObject.transform.parent.parent.GetComponent<InventorySlot>();
		if (cs == null) return;
		if (oldSlot.Empty || oldSlot.IsSellingMarketSlot || oldSlot.IsBuyingMarketSlot) return;
		bool move = (cs.name != oldSlot.name);
		inventory.SelectSlot = cs;
		if (move)
			if (cs.IsBuyingMarketSlot) {
				ControlSlotDataInMarket(oldSlot);
				return;
			}
			else if (cs.IsSellingMarketSlot) {
				ControlSlotDataInMarket(oldSlot);
				return;
			}
			else if (cs.isSpecial) {
				oldSlot.cotelController.AddItem(oldSlot.item);
				return;
			}
			else
				ExchangeSlotData(cs);
		if (cs.item == oldSlot.item && move) {
			if (cs.amount + oldSlot.amount <= oldSlot.item.max) {
				cs.amount += oldSlot.amount;
				cs.AmountText.text = cs.amount.ToString();
				NullifySlotData(oldSlot);
			}
			else {
				if (cs.item.max - cs.amount < oldSlot.amount) {
					int delta = cs.item.max - cs.amount;
					cs.amount += delta;
					oldSlot.amount -= delta;
				}
				else if (cs.item.max - cs.amount > oldSlot.amount) {
					cs.amount += oldSlot.amount;
					NullifySlotData(oldSlot);
				}
			}
		}
	}
	public void NullifySlotData(InventorySlot slot) {
		slot.item = null;
		slot.amount = 0;
		slot.Empty = true;
		slot.icon.color = new Color(1, 1, 1, 0);
		slot.icon.sprite = null;
		slot.AmountText.text = "";
	}
	public void ExchangeSlotData(InventorySlot newSlot) {
		ItemScriptableObject item = newSlot.item;
		int amount = newSlot.amount;
		bool isEmpty = newSlot.Empty;
		newSlot.item = oldSlot.item;
		newSlot.amount = oldSlot.amount;
		if (!oldSlot.Empty) newSlot.SetIcon();
		newSlot.Empty = oldSlot.Empty;
		oldSlot.item = item;
		oldSlot.amount = amount;
		if (!isEmpty) oldSlot.SetIcon();
		else {
			oldSlot.icon.color = new Color(1, 1, 1, 0);
			oldSlot.icon.sprite = null;
			oldSlot.AmountText.text = "";
		}
		oldSlot.Empty = isEmpty;
	}
	void ControlSlotDataInMarket(InventorySlot oldSlot) {//market
		inventory.coins += oldSlot.item.price;
		NullifySlotData(oldSlot);
	}
}