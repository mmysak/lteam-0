using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VirtualCursor : MonoBehaviour {
	public GameCore gameCore;
	private Vector3 last_position;
	private Vector3 current_position;
	public float Timer;
	private float _timer;
	public Scrollbar scrollbar;
	public Camera Camera;
	public static bool cursor_control;
	private Vector3 current_camera_rotation;
	private Vector3 last_camera_rotation;
	private void Awake() {
		Application.targetFrameRate = 60;
		StartCoroutine(CheckColl());
	}
	IEnumerator CheckColl() {
		while (gameCore.marketUI.gameObject.activeSelf||gameCore.szafaUI.gameObject.activeSelf) {
			yield return new WaitForSeconds(.1f);
		}
		while (scrollbar.size <= .9f) {
			if (gameCore.marketUI.gameObject.activeSelf || gameCore.szafaUI.gameObject.activeSelf)
				break;
			scrollbar.size += .1f;
			yield return new WaitForSeconds(.1f);
		}
		scrollbar.size = 0;
		cursor_control = gameCore.Interact();
		StartCoroutine(CheckColl());
		/*if (cursor_control) {
			print("001100");
			_timer = 0f;
		}
		else {
			print("Interact");
			_timer = 0f;
		}*/
	}
	void Update() {
		if (Input.GetKeyDown(KeyCode.R))
			cursor_control = false;
		current_camera_rotation = Camera.transform.rotation.eulerAngles.Round(0);
		current_position = transform.position.Round(1);
		if (last_position == current_position && last_camera_rotation == current_camera_rotation) {
			//print("Tick Tack");
			//_timer += Time.deltaTime;
			/*if (_timer >= Timer){
				if (cursor_control)
				{
					print("001100");
					_timer = 0f;
				}
				else
				{
					cursor_control = gameCore.Interact();
					print("Interact");
					_timer = 0f;
				}
			}*/
		}
		else {
			//print("Stop");
			_timer = 0f;
		}
		//Scrollbar.size = _timer/Timer;
		last_position = current_position;
		last_camera_rotation = current_camera_rotation;
	}
}