﻿using UnityEngine;
//[CreateAssetMenu(fileName = "build", menuName = "New building")]
public class ItemScriptableObject : ScriptableObject{
	public Sprite icon;
	public int max, id,price;
}