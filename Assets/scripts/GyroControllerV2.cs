using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroControllerV2 : MonoBehaviour {
	public VirtualCursor VirtualCursor;
	private bool _prev_cursor_control;
	public Camera Camera;
	private Quaternion _rot;
	private Gyroscope _gyro;
	private Vector3 _transformation_origin;
	void Awake() {
		_rot = new Quaternion(0, 0, 1, 0);
		_gyro = Input.gyro;
		_gyro.enabled = true;
	}
	void Update() {
		if (_prev_cursor_control != VirtualCursor.cursor_control) {
			_transformation_origin = (_gyro.attitude * _rot).eulerAngles;
		}
		if (VirtualCursor.cursor_control) {
			Vector3 transformation = (_gyro.attitude * _rot).eulerAngles;
			//VirtualCursor.transform.position = (_transformation_origin - transformation);
		}
		else {
			Quaternion transformation = _gyro.attitude * _rot;
			//print(_rot);
			Camera.transform.localRotation = transformation;
		}
		_prev_cursor_control = VirtualCursor.cursor_control;
	}
}