using UnityEngine;
using UnityEngine.UI;
public class InventorySlot : MonoBehaviour {
	public ItemScriptableObject item;
	public int amount;
	public bool Empty = true, IsHand, IsSellingMarketSlot, IsBuyingMarketSlot, isSpecial;
	public Image icon;
	public Text AmountText, priceText;
	public CotelController cotelController;
	public void SetIcon() {
		icon.color = new Color(1, 1, 1, 1);
		icon.sprite = item.icon;
		AmountText.text = amount.ToString();
	}
	public void Awake() {
		if ((IsBuyingMarketSlot || IsSellingMarketSlot) && item) {
			//priceText.text = item.price.ToString();
			SetIcon();
		}
	}
}